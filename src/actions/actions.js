import { createThunkAction, createAction } from "../Helpers";
import { getWeatherDataEndpoint,
     getLatestEntryPerRoomEndpoint,
     getWeatherDataBetweenEndpoint } from "../ApiEndpoints"

export const LOADING_DATA_STARTED_TYPE = "LOADING_DATA_STARTED_TYPE";
export const LOADING_DATA_ENDED_TYPE = "LOADING_DATA_ENDED_TYPE";

export const REPLACE_ALL_DATA_TYPE = "REPLACE_ALL_DATA_TYP";
export const getLast30Days = () => createThunkAction(REPLACE_ALL_DATA_TYPE,
    getWeatherDataEndpoint);
export const getWeatherDataBetween = (from, to) => createThunkAction(REPLACE_ALL_DATA_TYPE,
    getWeatherDataBetweenEndpoint(from, to));

export const GET_LATEST_DATA_TYPE = "GET_LATEST_DATA_TYPE";
export const getLatestData = () => createThunkAction(GET_LATEST_DATA_TYPE,
    getLatestEntryPerRoomEndpoint)

export const ADD_DISPLAY_ROOM_TYPE = "ADD_DISPLAY_ROOM_TYPE";
export const addDisplayRoom = room => createAction(ADD_DISPLAY_ROOM_TYPE, room);

export const REMOVE_DISPLAY_ROOM_TYPE = "REMOVE_DISPLAY_ROOM_TYPE";
export const removeDisplayRoom = room => createAction(REMOVE_DISPLAY_ROOM_TYPE, room);

export const DISPLAY_ALL_ROOMS_TYPE = "DISPLAY_ALL_ROOMS_TYPE";
export const displayAllRooms = () => createAction(DISPLAY_ALL_ROOMS_TYPE);
