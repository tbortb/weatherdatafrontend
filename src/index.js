import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk'
import reducer from "./reducers/dataReducer"
import { Provider } from 'react-redux';
import { getLatestData, getWeatherDataBetween } from "./actions/actions";

const store = createStore(reducer, applyMiddleware(thunkMiddleware));
store.dispatch(getLatestData());
//Get last 24 hours
store.dispatch(getWeatherDataBetween(
  Math.round((Date.now() - 86400 * 1000) / 1000),
  Math.round(new Date() / 1000)
));

ReactDOM.render(
  <Provider store={store}>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
