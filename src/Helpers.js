import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {LOADING_DATA_STARTED_TYPE, LOADING_DATA_ENDED_TYPE} from './actions/actions'

export const hookUpComponent = (stateToProps, actionCreators, component) => {

  const mapDispatchToProps = dispatch => bindActionCreators({
    ...actionCreators
  }, dispatch)

  return connect(
    stateToProps,
    mapDispatchToProps
  )(component);
}

export const createAction = (type, payload) => {
  return {
    type,
    payload
  }
}

export const createThunkAction = (type, apiEndpoint, data) => {
  return async (dispatch) => {
    dispatch({type: LOADING_DATA_STARTED_TYPE});
    let resp = await fetch(apiEndpoint, data);
    let payload = await resp.json();
    dispatch({
      type,
      payload
    });
    dispatch({type: LOADING_DATA_ENDED_TYPE})
  }
};