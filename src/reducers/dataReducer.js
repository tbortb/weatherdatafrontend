import {
    LOADING_DATA_STARTED_TYPE,
    LOADING_DATA_ENDED_TYPE,
    REPLACE_ALL_DATA_TYPE,
    ADD_DISPLAY_ROOM_TYPE,
    REMOVE_DISPLAY_ROOM_TYPE,
    GET_LATEST_DATA_TYPE,
    DISPLAY_ALL_ROOMS_TYPE
} from '../actions/actions'

const initialState = {
    loading_data: false,
    displayRooms: [],
    data: [],
    latestData: []
}

const mainReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOADING_DATA_STARTED_TYPE:
            return { ...state, loading_data: true };
        case LOADING_DATA_ENDED_TYPE:
            return { ...state, loading_data: false };
        case REPLACE_ALL_DATA_TYPE:
            return {
                ...state,
                data: action.payload.map(raw => ({
                    date: new Date(raw.timestamp * 1000),
                    ...raw
                }
                ))
            };
        case ADD_DISPLAY_ROOM_TYPE:
            const newState = {
                ...state,
                displayRooms: [...state.displayRooms, action.payload]
            };
            return newState;
        case REMOVE_DISPLAY_ROOM_TYPE:
            const removedState = {
                ...state,
                displayRooms: [...state.displayRooms.filter(r => r !== action.payload)]
            };
            return removedState;
        case DISPLAY_ALL_ROOMS_TYPE:
            return {...state,
                displayRooms: Array.from(new Set(state.data.map(d => d.room)))
            };
        case GET_LATEST_DATA_TYPE:
            return {
                ...state, latestData: action.payload.map(raw => ({
                    date: new Date(raw.timestamp * 1000),
                    ...raw
                }
                ))
            };
        default:
            return state;
    }
}

export default mainReducer;