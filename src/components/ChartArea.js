import React from 'react';
import { Chart } from "chart.js";
import 'chartjs-adapter-moment';
import { hookUpComponent } from "../Helpers"

import {
  ArcElement,
  LineElement,
  BarElement,
  PointElement,
  BarController,
  BubbleController,
  DoughnutController,
  LineController,
  PieController,
  PolarAreaController,
  RadarController,
  ScatterController,
  CategoryScale,
  LinearScale,
  LogarithmicScale,
  RadialLinearScale,
  TimeScale,
  TimeSeriesScale,
  Decimation,
  Filler,
  Legend,
  Title,
  Tooltip
} from 'chart.js';

Chart.register(
  ArcElement,
  LineElement,
  BarElement,
  PointElement,
  BarController,
  BubbleController,
  DoughnutController,
  LineController,
  PieController,
  PolarAreaController,
  RadarController,
  ScatterController,
  CategoryScale,
  LinearScale,
  LogarithmicScale,
  RadialLinearScale,
  TimeScale,
  TimeSeriesScale,
  Decimation,
  Filler,
  Legend,
  Title,
  Tooltip
  );
  
  class ChartArea extends React.Component {
    
    constructor(props) {
      super(props);
      this.state = this.initialState;
    }
    
    initialState = { chart: {} };
    chartRef = React.createRef();

  componentDidMount() {
    const chart = this.createNewChart();
    this.setState({chart});
  }

  createNewChart(datasets){
    const myChartRef = this.chartRef.current.getContext("2d");
    return new Chart(myChartRef, {
      type: 'scatter',
      data: { datasets },
      options: {
        scales: {
          x: {
            type: 'timeseries'
          }
        }
      }
    });
  }

  componentWillReceiveProps(nextProps){
    this.state.chart.destroy();
      
      //Create one dataset for each room to be displayed
      let datasets = [];
      let allowedColors = ["#73ecfa", "#be7cfc", "#f759ab", "#ff9a63", "#78ff7a", "#fff763"];
      let index = 0;
      for (let room of nextProps.displayRooms) {
        let data = nextProps.data
        .filter(d => d.room === room)
        .map(d => ({ x: d.date, y: d[nextProps.show] }));
        let newSeries = {
          label: room,
          borderColor: allowedColors[index],
          backgroundColor: allowedColors[index],
          data
        };
        datasets.push(newSeries);
        index++;
      }
      let newChart = this.createNewChart(datasets);
      
      this.setState({chart: newChart});
  }


  render() {
    return <h3>{this.props.chartTitle}
      <canvas
        id="myChart"
        ref={this.chartRef}
      />
    </h3>

  }
}

const mapStateToProps = (state, ownProps) => {
  return ({
    data: state.data,
    loading_data: state.loading_data,
    displayRooms: state.displayRooms,
    show: ownProps.show,
    chartTitle: ownProps.chartTitle
  })
}

export default hookUpComponent(mapStateToProps, {}, ChartArea);