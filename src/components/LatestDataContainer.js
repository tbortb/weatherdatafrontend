import React from 'react';
import { hookUpComponent } from "../Helpers"
import SimpleDataEntry from "./SimpleDataEntry"

class LatestDataContainer extends React.Component {

    render = () =>{
     return <span style={{display: "flex", flexDirection: "row"}}>
        {this.props.latestData
            .filter(l => this.props.displayRooms.includes(l.room))
            .map((value, index) => <SimpleDataEntry key={index} data={value}> </SimpleDataEntry>)}
    </span>
    }
}

const mapStateToProps = (state, ownProps) => {
    return ({
        displayRooms: state.displayRooms,
        latestData: state.latestData
    })
}

export default hookUpComponent(mapStateToProps,
    {},
    LatestDataContainer);