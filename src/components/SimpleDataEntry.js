const SimpleDataEntry = props => {

    let warning = new Date() - props.data.date > 2 * 60 * 60 * 1000 ? "Old Data Warning" : "";

    return <div style={{
        marginLeft: "20px",
        padding: "15px",
        color: "grey",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        borderStyle: "solid",
        borderColor: "grey"
    }}>
        <h4>{props.data.room}</h4>
        <div>{props.data.date.toLocaleString()}</div>
        <div>{props.data.temperatureCelsius} °C</div>
        <div>{props.data.relHumidity} %</div>
        <div>{props.data.absHumidity} mg/L</div>
        <div style={{color: "red"}}>{warning}</div>
    </div>
}

export default SimpleDataEntry;