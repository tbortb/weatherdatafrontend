import React from 'react';
import { getLast30Days,
    addDisplayRoom,
    removeDisplayRoom,
    getLatestData,
    getWeatherDataBetween,
    displayAllRooms } from '../actions/actions';
import { hookUpComponent } from "../Helpers"

class FilterSettings extends React.Component {

    getData = () => this.props.getLast30Days();
    getLatest = () => this.props.getLatestData();
    getLatest24Hours = () => {
        const twentyFourHoursAgo = Math.round((Date.now() - 86400 * 1000) / 1000);
        const now = Math.round(new Date() / 1000);
        this.props.getWeatherDataBetween(twentyFourHoursAgo, now);
    }

    toggleRoom = e => {
        let room = e.target.value;
        if (e.target.checked) {
            this.props.addDisplayRoom(room);
        } else {
            this.props.removeDisplayRoom(room);
        }
    }

    toggleAllRooms = e => {
        if (e.target.checked) {
            this.props.displayAllRooms();
        }else{
            this.props.allRooms.forEach(room => this.props.removeDisplayRoom(room));
        }
    }


    render() {
        return (<span>
            <div id="checkboxes">
                <h3>Choose Rooms</h3>
                
                <input type="checkbox"
                onChange={this.toggleAllRooms}
                checked={this.props.allRooms.every(room => this.props.displayRooms.includes(room))}/>All rooms

                {this.props.allRooms.map((room, index) => (
                    <div key={index}>
                        <input type="checkbox"
                        onChange={this.toggleRoom}
                        value={room}
                        checked={this.props.displayRooms.includes(room)} />
                        {room}
                    </div>
                ))}

            </div>
            <p></p>
            <button onClick={this.getData}>Load last 30 days</button>
            <button onClick={this.getLatest}>Get latest data</button>
            <button onClick={this.getLatest24Hours}>Only latest 24 hours</button>
        </span>);
    }
}

const mapStateToProps = (state, ownProps) => {
    return ({
        data: state.data,
        allRooms: Array.from(new Set(state.data.map(d => d.room))),
        displayRooms: state.displayRooms
    })
}

export default hookUpComponent(mapStateToProps,
    {
        getLast30Days,
        addDisplayRoom,
        removeDisplayRoom,
        getLatestData,
        getWeatherDataBetween,
        displayAllRooms
    },
    FilterSettings);