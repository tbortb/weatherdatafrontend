import './App.css';
import FilterSettings from "./components/FilterSettings";
import ChartArea from "./components/ChartArea";
import LatestDataContainer from "./components/LatestDataContainer";

function App() {
  return (
    <div>
        <FilterSettings />
        <p/>
        <LatestDataContainer />
        <ChartArea show="temperatureCelsius" chartTitle="Temperature in Celsius"/>
        <ChartArea show="relHumidity" chartTitle="Relative Humidity in %"/>
        <ChartArea show="absHumidity" chartTitle="Absolute Humidity in mg/L"/>
    </div>
  );
}

export default App;
