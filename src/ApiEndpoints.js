
const baseRoute = process.env.REACT_APP_BASE_ROUTE;

export const getWeatherDataEndpoint = baseRoute;
export const getWeatherDataBetweenEndpoint = (from, to) => baseRoute + "?from=" + from + "&to=" + to;
export const getLatestEntryPerRoomEndpoint = baseRoute + "/latest";